package metrics

import (
	"testing"

	assert "github.com/stretchr/testify/assert"
	suite "github.com/stretchr/testify/suite"
	zap "go.uber.org/zap"
)

type InfluxdbReporterTestSuite struct {
	suite.Suite
}

func (suite *InfluxdbReporterTestSuite) TestMergeDoesntFailWithNoOrigin() {
	merge(make(map[string]string), nil)
}

func (suite *InfluxdbReporterTestSuite) TestMergeDoesntFailWithNoDestination() {
	merge(make(map[string]string), nil)
}

func (suite *InfluxdbReporterTestSuite) TestWritesOriginToDestination() {
	origin := make(map[string]string)
	destination := make(map[string]string)
	destination["tag1"] = "value1"
	origin["tag2"] = "value2"
	origin["tag1"] = "value3"
	merge(destination, origin)
	assert.Equal(suite.T(), "value2", destination["tag2"])
	assert.Equal(suite.T(), "value3", destination["tag1"])
}

func (suite *InfluxdbReporterTestSuite) TestExtractTagsNoTags() {
	reporter := reporter{logger: zap.L()}
	name, tags := reporter.extractTags("somename")
	assert.Equal(suite.T(), "somename", name)
	assert.Empty(suite.T(), tags)
}

func (suite *InfluxdbReporterTestSuite) TestExtractTagsEmptyTags() {
	reporter := reporter{logger: zap.L()}
	name, tags := reporter.extractTags("somename;;;{}")
	assert.Equal(suite.T(), "somename", name)
	assert.Empty(suite.T(), tags)
}

func (suite *InfluxdbReporterTestSuite) TestExtractTagsWithTags() {
	reporter := reporter{logger: zap.L()}
	name, tags := reporter.extractTags(`somename;;;{"tag1":"value1"}`)
	assert.Equal(suite.T(), "somename", name)
	assert.Equal(suite.T(), map[string]string{
		"tag1": "value1",
	}, tags)
}

func TestInfluxdbReporterSuit(t *testing.T) {
	suite.Run(t, new(InfluxdbReporterTestSuite))
}
