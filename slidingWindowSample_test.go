package metrics

import (
	"testing"
	"time"

	assert "github.com/stretchr/testify/assert"
	suite "github.com/stretchr/testify/suite"
)

type SlidingWindowTestSuite struct {
	suite.Suite
}

func (suite *SlidingWindowTestSuite) TestTrimmingOfValues() {
	sample := NewSlidingWindowSample(time.Minute)

	sample.Update(1)
	time.Sleep(time.Nanosecond)
	sample.Update(2)
	time.Sleep(time.Nanosecond)
	t := time.Now().UnixNano()
	time.Sleep(time.Nanosecond)
	sample.Update(3)
	time.Sleep(time.Nanosecond)
	sample.Update(4)

	assert.Equal(suite.T(), int64(4), sample.Count())
	(sample.(*SlidingWindowSample)).trim((time.Duration(t) + time.Minute).Nanoseconds())
	assert.Equal(suite.T(), int64(2), sample.Count())
	assert.Contains(suite.T(), sample.Values(), int64(3))
	assert.Contains(suite.T(), sample.Values(), int64(4))
}

func TestSlidingWindowSuit(t *testing.T) {
	suite.Run(t, new(SlidingWindowTestSuite))
}
