package metrics

import (
	"testing"

	assert "github.com/stretchr/testify/assert"
	suite "github.com/stretchr/testify/suite"
)

type FunctionsTestSuite struct {
	suite.Suite
}

func (suite *FunctionsTestSuite) TestReturnsNameIfNoTags() {
	val := mergeNameAndTags("somename", nil)
	assert.Equal(suite.T(), "somename", val)
}

func (suite *FunctionsTestSuite) TestJsonEncodesTags() {
	tags := make(map[string]string)
	tags["tag1"] = "value1"
	val := mergeNameAndTags("somename", tags)
	assert.Equal(suite.T(), `somename;;;{"tag1":"value1"}`, val)
}

func (suite *FunctionsTestSuite) TestHandlesEmptyTags() {
	tags := make(map[string]string)
	val := mergeNameAndTags("somename", tags)
	assert.Equal(suite.T(), "somename;;;{}", val)
}

func TestFunctionsSuit(t *testing.T) {
	suite.Run(t, new(FunctionsTestSuite))
}
